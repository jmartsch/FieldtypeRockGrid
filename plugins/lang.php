<?php namespace ProcessWire;

$this->rg->x('show', __('show'));
$this->rg->x('trash', __('trash'));
$this->rg->x('choose', __('choose'));
$this->rg->x('refreshTimer', __('Refresh every ... seconds'));